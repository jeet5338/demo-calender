import { Button, Grid, makeStyles, TextField } from "@material-ui/core";
import React, { useState } from "react";

/**
 * @author
 * @function Dates
 **/

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 155,
  },
}));

const AddEvent = (props) => {
  const classes = useStyles();
  const [title, settitle] = useState("");
  const [desc, setdesc] = useState("");
  const [place, setplace] = useState("");
  const [date, setdate] = useState("");
  const [from, setfrom] = useState("09:30");
  const [to, setto] = useState("12:00");

  const handleAddEvent = () => {
    let event = {
      title: title,
      description: desc,
      location: place,
      date: date,
      from: from,
      to: to,
    };
    console.log(event);
    props.handleAddEvent(event);
  };

  return (
    <Grid container spacing={1}>
      <Grid item>
        <TextField
          id="standard-basic"
          label="Title"
          className={classes.textField}
          value={title}
          onChange={(e) => settitle(e.target.value)}
        />
      </Grid>
      <Grid item>
        <TextField
          id="standard-basic"
          label="Description"
          className={classes.textField}
          value={desc}
          onChange={(e) => setdesc(e.target.value)}
        />
      </Grid>
      <Grid item>
        <TextField
          id="standard-basic"
          label="Place"
          className={classes.textField}
          value={place}
          onChange={(e) => setplace(e.target.value)}
        />
      </Grid>
      <Grid item>
        <TextField
          id="date"
          label="Date"
          type="date"
          value={date}
          onChange={(e) => setdate(e.target.value)}
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
        />
      </Grid>
      <Grid item>
        <TextField
          id="time"
          label="From"
          type="time"
          value={from}
          onChange={(e) => setfrom(e.target.value)}
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
      </Grid>
      <Grid item>
        <TextField
          id="time"
          label="To"
          type="time"
          value={to}
          onChange={(e) => setto(e.target.value)}
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
        />
      </Grid>
      <Button
        type="primary"
        onClick={handleAddEvent}
        color="primary"
        variant="contained"
      >
        Add Event
      </Button>
    </Grid>
  );
};

export default AddEvent;
