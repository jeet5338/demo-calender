import { Button, Grid, makeStyles, Paper } from "@material-ui/core";
import React from "react";

/**
 * @author
 * @function Time
 **/

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

const Time = (props) => {
  const classes = useStyles();
  const times = [
    "12 AM",
    "1 AM",
    "2 AM",
    "3 AM",
    "4 AM",
    "5 AM",
    "6 AM",
    "7 AM",
    "8 AM",
    "9 AM",
    "10 AM",
    "11 AM",
    "12 PM",
    "1 PM",
    "2 PM",
    "3 PM",
    "4 PM",
    "5 PM",
    "6 PM",
    "7 PM",
    "8 PM",
    "9 PM",
    "10 PM",
    "11 PM",
  ];
  return (
    <Grid container spacing={1}>
      {times.map((time, index) => {
        return (
          <Grid item xs={12} key={index}>
            <Paper className={classes.paper}>{time}</Paper>
          </Grid>
        );
      })}
    </Grid>
  );
};

export default Time;
