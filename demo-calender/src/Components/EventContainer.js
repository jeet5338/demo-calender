import { Button, Grid, makeStyles, Paper } from "@material-ui/core";
import React from "react";
import Events from "./Events";
import Time from "./Time";

/**
 * @author
 * @function EventContainer
 **/

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

const EventContainer = (props) => {
  const classes = useStyles();
  return (
    <Grid container spacing={1}>
      <Grid item xs={1}>
        <Paper className={classes.paper}>
          <Time />
        </Paper>
      </Grid>
      <Grid item xs={11}>
        <Paper className={classes.paper}>
          <Events eventList={props.eventList} />
        </Paper>
      </Grid>
    </Grid>
  );
};

export default EventContainer;
