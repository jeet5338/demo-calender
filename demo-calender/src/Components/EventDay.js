import { Grid, makeStyles, Modal, Paper } from "@material-ui/core";
import React, { useEffect, useState } from "react";

/**
 * @author
 * @function EventDay
 **/

function getModalStyle() {
  return {
    top: `50%`,
    left: `50%`,
  };
}
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    width: "135px",
  },
  vacant: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    width: "135px",
    height: "21px",
  },
  body: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const EventDay = (props) => {
  const classes = useStyles();
  const [eventDayList, setEventDayList] = useState([]);
  const [modalStyle] = useState(getModalStyle);
  const [open, setOpen] = useState("");
  const handleOpen = (event) => {
    console.log(event);
    setOpen(event);
  };

  const handleClose = () => {
    setOpen("");
  };
  useEffect(() => {
    let list = [];
    for (let index = 0; index < 24; index++) {
      list.push("");
    }
    props.eventList.forEach((event) => {
      let from = parseInt(event.from.split(":")[0]);
      let to =
        parseInt(event.to.split(":")[0]) +
        (event.to.split(":")[1] === "00" ? 0 : 1);
      for (let index = from; index < to; index++) {
        list[index] = event;
      }
      setEventDayList(list);
    });
  }, [props.eventList]);
  const body = (event) => {
    return (
      <div style={modalStyle} className={classes.body}>
        <h2 id="simple-modal-title">{event.title}</h2>
        <p id="simple-modal-description">{event.description}</p>
        <p>Date : {event.date},</p>
        <p>Location : {event.location}</p>
        <p>
          From : {event.from} - To : {event.to}
        </p>
      </div>
    );
  };
  return (
    <div>
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        spacing={1}
      >
        {eventDayList.map((event, index) => {
          return (
            <Grid item key={index}>
              {event !== "" ? (
                <Paper
                  className={classes.paper}
                  onClick={() => handleOpen(event)}
                >
                  {event.title}
                </Paper>
              ) : (
                <div className={classes.vacant}>-</div>
              )}
            </Grid>
          );
        })}
      </Grid>
      <Modal
        open={open !== ""}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body(open)}
      </Modal>
    </div>
  );
};

export default EventDay;
