import { Button, Grid, makeStyles, Paper } from "@material-ui/core";
import React from "react";
import EventDay from "./EventDay";

/**
 * @author
 * @function Events
 **/

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    width: "135px",
  },
}));

const Events = (props) => {
  const classes = useStyles();
  const week = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
  return (
    <Grid container spacing={1}>
      {week.map((day, index) => {
        return (
          <Grid item key={index}>
            <EventDay eventList={props.eventList} />
          </Grid>
        );
      })}
    </Grid>
  );
};

export default Events;
