import { Button, Grid, makeStyles, Paper } from "@material-ui/core";
import React from "react";

/**
 * @author
 * @function Dates
 **/

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    width: "135px",
  },
}));

const Dates = (props) => {
  const classes = useStyles();
  const week = [
    new Date().getDate() - 3 + " Nov",
    new Date().getDate() - 2 + " Nov",
    new Date().getDate() - 1 + " Nov",
    new Date().getDate() + " Nov",
    new Date().getDate() + 1 + " Nov",
    new Date().getDate() + 2 + " Nov",
    new Date().getDate() + 3 + " Nov",
  ];
  return (
    <Grid container spacing={1}>
      {week.map((day, index) => {
        return (
          <Grid item key={index}>
            <Paper className={classes.paper}>{day}</Paper>
          </Grid>
        );
      })}
    </Grid>
  );
};

export default Dates;
