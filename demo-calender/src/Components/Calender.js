import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Dates from "./Dates";
import EventContainer from "./EventContainer";
import AddEvent from "./AddEvent";
import { Button } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  paperTime: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

export default function Calender() {
  const classes = useStyles();
  const [eventList, setEventList] = useState([
    {
      title: "Event1",
      description: "This is a demo Event",
      location: "place room 1",
      date: "12/11/2020",
      from: "2:00",
      to: "3:00",
    },
    {
      title: "Event2",
      description: "This is a demo Event",
      location: "place room 1",
      date: "13/11/2020",
      from: "11:00",
      to: "13:00",
    },
    {
      title: "Event3",
      description: "This is a demo Event",
      location: "place room 1",
      date: "14/11/2020",
      from: "9:00",
      to: "10:00",
    },
    {
      title: "Event4",
      description: "This is a demo Event",
      location: "place room 1",
      date: "12/11/2020",
      from: "17:00",
      to: "19:00",
    },
  ]);

  const handleAddEvent = (event) => {
    setEventList([...eventList, event]);
  };
  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <AddEvent handleAddEvent={handleAddEvent} />
          </Paper>
        </Grid>
        <Grid item xs={1}>
          <Paper className={classes.paperTime}>Time</Paper>
        </Grid>
        <Grid item xs={11} style={{ paddingLeft: "14px" }}>
          <Dates />
        </Grid>
        <Grid item xs={12}>
          <EventContainer eventList={eventList} />
        </Grid>
      </Grid>
    </div>
  );
}
